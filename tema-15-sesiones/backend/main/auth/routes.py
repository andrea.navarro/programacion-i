from flask import request, jsonify, Blueprint
from .. import db
from main.models import ProfessorModel
from flask_jwt_extended import jwt_required, get_jwt_identity, create_access_token
#Importar funcion de envío de mail
from main.mail.functions import sendMail

auth = Blueprint('auth', __name__, url_prefix='/auth')

@auth.route('/login', methods=['POST'])
def login():
    professor = db.session.query(ProfessorModel).filter(ProfessorModel.email == request.get_json().get("email")).first_or_404()
    if professor.validate_pass(request.get_json().get("password")):
        access_token = create_access_token(identity=professor)
        data = {
            'id': str(professor.id),
            'email': professor.email,
            'access_token': access_token
        }

        return data, 200
    else:
        return 'Incorrect password', 401

@auth.route('/register', methods=['POST'])
def register():
    professor = ProfessorModel.from_json(request.get_json())
    exists = db.session.query(ProfessorModel).filter(ProfessorModel.email == professor.email).scalar() is not None
    if exists:
        return 'Duplicated mail', 409
    else:
        try:
            db.session.add(professor)
            db.session.commit()
            #Enviar mail de bienvenida
            sent = sendMail([professor.email],"Welcome!",'register',professor = professor)
        except Exception as error:
            db.session.rollback()
            return str(error), 409
        return professor.to_json() , 201
