from flask import Blueprint, render_template, current_app, redirect, url_for, current_app,  request
from ..forms.auth_forms import  LoginForm
# Importar objeto Form
from ..forms.project_form import ProjectForm
# Importar librería request para realizar consulta y json para manejar la estructura de datos
import requests, json
from flask import make_response
from flask_login import login_required, LoginManager, current_user
from .auth import admin_required


project = Blueprint('project', __name__, url_prefix='/project')

@project.route('/')
@login_required
def index():
    data = {}
    data['page'] = 1
    #Obtener token
    auth = request.cookies['access_token']
    #Crear headers
    headers = {
    'content-type': "application/json",
    'authorization': "Bearer "+auth
    }
    #Obtener proyects
    r = requests.get(
        current_app.config["API_URL"]+'/projects',
        headers = headers,
        data = json.dumps(data))
    #Cargar proyectos
    projects = json.loads(r.text)["projects"]
    #Mostrar template
    return render_template('project_list.html', projects=projects)

@project.route('/view/<int:id>')
@login_required
def view(id):
    #Obtener token
    auth = request.cookies['access_token']
    #Crear headers
    headers = {
    'content-type': "application/json",
    'authorization': "Bearer "+auth
    }
    r = requests.get(
        current_app.config["API_URL"]+'/project/'+str(id),
        headers=headers)
    if(r.status_code==404):
        return redirect(url_for('project.index'))
    project = json.loads(r.text)
    #Mostrar template
    return render_template('project_view.html', project=project)

@project.route('/create', methods=["POST","GET"])
@login_required
@admin_required
def create():
    #Instanciar formulario
    form = ProjectForm()
    # Obtener profesores
    data = {}
    data['page'] = 1
    #Obtener token
    auth = request.cookies['access_token']
    #Crear headers
    headers = {
    'content-type': "application/json",
    'authorization': "Bearer "+auth
    }
    r = requests.get(
            current_app.config["API_URL"]+'/professors',
            headers=headers,
            data = json.dumps(data))
    professors = [(item['id'], item['lastname']+" "+item['firstname']) for item in json.loads(r.text)["professors"]]
    inscriptions_p = [(item['id'], item['lastname']+" "+item['firstname']) for item in json.loads(r.text)["professors"]]

    # Cargar profesores en campo de formulario
    form.professorId.choices = professors
    inscriptions_p.insert(0, (0, ""))
    form.professorInscr1Id.choices = inscriptions_p
    form.professorInscr2Id.choices = inscriptions_p
    form.professorInscr3Id.choices = inscriptions_p
    if form.validate_on_submit(): #Si el formulario ha sido enviado y es validado correctamente
        # Cargar datos del formulario
        data = {}
        data["name"] = form.name.data
        data["year"] = form.year.data.year
        data["professorId"] = form.professorId.data
        inscriptions = []
        if form.professorInscr1Id.data != 0:
            inscriptions.append({"professorId":form.professorInscr1Id.data})
        if form.professorInscr2Id.data != 0:
            inscriptions.append({"professorId":form.professorInscr2Id.data})
        if form.professorInscr3Id.data != 0:
            inscriptions.append({"professorId":form.professorInscr3Id.data})
        data["inscriptions"] = inscriptions
        # Crear solicitud POST
        r = requests.post(
            current_app.config["API_URL"]+'/projects',
            headers = headers,
            data = json.dumps(data))
        if(r.status_code==201):
            return redirect(url_for('project.index')) #Redirecciona a lista
    return render_template('project_form.html', form = form) #Muestra el formulario
