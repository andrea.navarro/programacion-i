from flask import Blueprint, render_template, redirect, url_for, current_app, request
from ..forms.auth_forms import  LoginForm
# Importar objeto Form
from ..forms.professor_form import ProfessorForm
from flask_login import login_required, LoginManager,current_user
# Importar librería request para realizar consulta y json para manejar la estructura de datos
import requests, json
from .auth import admin_required

#Crear Blueprint
professor = Blueprint('professor', __name__, url_prefix='/professor')

@professor.route('/')
def index():
    loginForm = LoginForm()
    data = {}
    #Página por defecto
    data['page'] = 1
    #Cantidad de elementos por página para esta tabla
    data['per_page'] = 4
     #Número de página
    if 'page' in request.args:
        #Si se han usado los botones de paginación cargar nueva página
        data["page"] = request.args.get('page','')
    #Crear headers
    headers = {
    'content-type': "application/json"
    }

    # Generar consulta GET al endpoint
    r = requests.get(
        current_app.config["API_URL"]+'/professors',
        headers=headers,
        data = json.dumps(data))
    #Convertir respuesta de JSON a  diccionario
    professors = json.loads(r.text)["professors"]
    #Cargar datos de paginación de API
    pagination = {}
    # Obtener cantidad de páginas
    pagination["pages"] = json.loads(r.text)["pages"]
    # Obtener página actual
    pagination["current_page"] = json.loads(r.text)["page"]
    #Mostrar template
    return render_template('professor_list.html', professors=professors, loginForm = loginForm, pagination= pagination )

@professor.route('/view/<int:id>')
@login_required
def view(id):
    # Generar consulta GET al endpoint
    #Obtener token
    auth = request.cookies['access_token']
    #Crear headers
    headers = {
    'content-type': "application/json",
    'authorization': "Bearer "+auth
    }
    r = requests.get(
        current_app.config["API_URL"]+'/professor/'+str(id),
        headers=headers)
    # Verificar código de respuesta
    if(r.status_code==404):
        # Si el recurso no existe redireccionar
        return redirect(url_for('professor.index'))
    #Convertir respuesta de JSON a  diccionario
    professor = json.loads(r.text)
    #Mostrar template
    return render_template('professor_view.html',professor=professor  )

@professor.route('/create', methods=["POST","GET"])
@login_required
@admin_required
def create():
    form = ProfessorForm() #Instanciar formulario
    if form.validate_on_submit(): #Si el formulario ha sido enviado y es validado correctamente
        data = {}
        data["firstname"] = form.firstname.data
        data["lastname"] = form.lastname.data
        data["email"] = form.email.data
        data["password"] = form.password.data
        data["fechaNac"] = form.fechaNac.data.strftime("%Y-%m-%d")
        #Obtener token
        auth = request.cookies['access_token']
        #Crear headers
        headers = {
        'content-type': "application/json",
        'authorization': "Bearer "+auth
        }
        r = requests.post(
            current_app.config["API_URL"]+'/professors',
            headers = headers,
            data = json.dumps(data))
        return redirect(url_for('professor.index')) #Redirecciona a lista
    return render_template('professor_form.html', form = form) #Muestra el formulario
