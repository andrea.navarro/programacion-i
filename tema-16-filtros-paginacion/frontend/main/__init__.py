import os
from flask import Flask
from dotenv import load_dotenv
from flask_wtf import CSRFProtect #importar para proteccion CSRF
# Importar LoginManager
from flask_login import LoginManager

# Inicializar módulos CSRF
csrf = CSRFProtect()
# Inicializar LoginManager
login_manager = LoginManager()

def create_app():
    app = Flask(__name__)
    load_dotenv()
    # Importar URL de la API a la que debe conectarse
    app.config['API_URL'] = os.getenv('API_URL')
    # Importar clave secreta
    app.config["SECRET_KEY"] = os.getenv('SECRET_KEY')
    #Inicializar protección csrf
    csrf.init_app(app)
    login_manager.init_app(app)
    #Importar Blueprints
    from main.routes import main, professor, project
    app.register_blueprint(routes.main.main)
    app.register_blueprint(routes.professor.professor)
    app.register_blueprint(routes.project.project)
    return app
