from .. import db
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

class Professor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    fechaNac = db.Column(db.DateTime, nullable=False)
    #Mail usado como nombre de usuario
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    #Contraseña que será el hash de la pass en texto plano
    password = db.Column(db.String(128), nullable=False)
    #Rol (En el caso que existan diferentes tipos de usuarios con diferentes permisos)
    role = db.Column(db.String(10), nullable=False, default="user")
    #Relación
    projects = db.relationship("Project", back_populates="professor",cascade="all, delete-orphan")
    inscriptions = db.relationship("Inscription", back_populates="professor",cascade="all, delete-orphan")

    #Getter de la contraseña plana no permite leerla
    @property
    def plain_password(self):
        raise AttributeError('Password cant be read')
    #Setter de la contraseña toma un valor en texto plano
    # calcula el hash y lo guarda en el atributo password
    @plain_password.setter
    def plain_password(self, password):
        self.password = generate_password_hash(password)
    #Método que compara una contraseña en texto plano con el hash guardado en la db
    def validate_pass(self,password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<Professor: %r %r >' % (self.firstname, self.lastname)
    #Convertir objeto en JSON
    def to_json(self):
        professor_json = {
            'id': self.id,
            'firstname': str(self.firstname),
            'lastname': str(self.lastname),
            'fechaNac': str(self.fechaNac.strftime("%d-%m-%Y")),
            'email': str(self.email),

        }
        return professor_json

    def to_json_complete(self):
        projects = [project.to_json_public() for project in self.projects]
        professor_json = {
            'id': self.id,
            'firstname': str(self.firstname),
            'lastname': str(self.lastname),
            'fechaNac': str(self.fechaNac.strftime("%d-%m-%Y")),
            'email': str(self.email),
            'projects':projects

        }
        return professor_json


    @staticmethod
    #Convertir JSON a objeto
    def from_json(professor_json):
        id = professor_json.get('id')
        firstname = professor_json.get('firstname')
        lastname = professor_json.get('lastname')
        fechaNac = datetime.strptime(professor_json.get('fechaNac'), '%Y-%m-%d')
        email = professor_json.get('email')
        password = professor_json.get('password')
        role = professor_json.get('role')
        return Professor(id=id,
                    firstname=firstname,
                    lastname=lastname,
                    fechaNac=fechaNac,
                    email=email,
                    plain_password=password,
                    role=role,
                    )

class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    year = db.Column(db.Integer, nullable=False)
    #Campo clave foranea
    professorId = db.Column(db.Integer, db.ForeignKey('professor.id'), nullable=False)
    #Relación
    professor = db.relationship('Professor',back_populates="projects",uselist=False,single_parent=True)
    inscriptions = db.relationship("Inscription", back_populates="project",cascade="all, delete-orphan")
    def __repr__(self):
        return '<Project: %r %r >' % (self.name, self.year)

    #Convertir objeto en JSON
    def to_json(self):
        self.professor = db.session.query(Professor).get_or_404(self.professorId)
        inscriptions = [inscription.to_json() for inscription in self.inscriptions]
        project_json = {
            'id': self.id,
            'name': str(self.name),
            'year': str(self.year),
            'professor': self.professor.to_json(),
            'inscriptions': inscriptions
        }
        return project_json

    def to_json_public(self):
        project_json = {
            'id': self.id,
            'name': str(self.name),
            'year': str(self.year)
        }
        return project_json

    @staticmethod
    #Convertir JSON a objeto
    def from_json(project_json):
        id = project_json.get('id')
        name = project_json.get('name')
        year = project_json.get('year')
        professorId =  project_json.get('professorId')
        return Project(id=id,
                    name=name,
                    year=year,
                    professorId = professorId
                    )
    @staticmethod
    #Convertir JSON a objeto
    def from_json_inscriptions(project_json):
        inscriptions = []
        for inscription in project_json.get('inscriptions'):
            inscriptions.append(Inscription(professorId = inscription["professorId"]))
        id = project_json.get('id')
        name = project_json.get('name')
        year = project_json.get('year')
        professorId =  project_json.get('professorId')
        return Project(id=id,
                    name=name,
                    year=year,
                    professorId = professorId,
                    inscriptions = inscriptions
                    )

class Inscription(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    professorId = db.Column(db.Integer, db.ForeignKey('professor.id'), nullable=False)
    professor = db.relationship('Professor',back_populates="inscriptions",uselist=False,single_parent=True)
    projectId = db.Column(db.Integer, db.ForeignKey('project.id'), nullable=False)
    project= db.relationship('Project',back_populates="inscriptions",uselist=False,single_parent=True)
    def __repr__(self):
        return '<Inscription: %r %r >' % (self.professor, self.project)

    #Convertir objeto en JSON
    def to_json(self):
        self.professor = db.session.query(Professor).get_or_404(self.professorId)
        self.project = db.session.query(Project).get_or_404(self.projectId)
        inscription_json = {
            'id': self.id,
            'professor': self.professor.to_json(),
            'project': self.project.to_json_public()
        }
        return inscription_json

    @staticmethod
    #Convertir JSON a objeto
    def from_json(inscription_json):
        id = inscription_json.get('id')
        professorId =  inscription_json.get('professorId')
        projectId =  inscription_json.get('projectId')
        return Inscription(id=id,
                    professorId = professorId,
                    projectId = projectId
                    )
