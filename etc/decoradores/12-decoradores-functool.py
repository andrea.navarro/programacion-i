from functools import wraps

def saludar(funcion):
	#Utilización del decorador wraps
	@wraps(funcion)
	def saludo(*args, **kwargs):
		print("Buenos días")
		funcion(*args, **kwargs)
		print("Hasta luego!")
	return saludo

@saludar
def presentarse(nombre):
    print("Mi nombre es "+nombre)
print("El nombre de la función es " + presentarse.__name__)
