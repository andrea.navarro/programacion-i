#Definir la función saludar que toma como parámetro un nombre
def saludar(nombre):
	#Definir función interna que toma como parámetro un valor bool
	def saludo(dia):
		if(dia):
			mensaje = "Buenos días "
		else:
			mensaje = "Buenas noches "
		return  mensaje+ nombre+"!!"
	#Devolver función interna
	return saludo

#Igualar la variable saludoPedro a la función 'saludo' que retorna la función 'saludar'	con el parámetro nombre igualado a Pedro
saludoPedro = saludar("Pedro")

#Ejecutar la función asignada pasando por parámetro el valor de 'día' como verdadero
print(saludoPedro(True))
