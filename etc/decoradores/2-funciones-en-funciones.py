#Definir función
def saludar(nombre):
	#Definir función interna
    def saludo(nombre):
		#Implementación de la función interna 'saludo'
        return "Hola "+nombre+" "
    #Implementación de la función saludar
    print(saludo(nombre)+"encantado de conocerte")

saludar("María")
#Esta linea genera error ya que no se puede acceder a la función saludo desde afuera de la función saludar
#saludo("María")
