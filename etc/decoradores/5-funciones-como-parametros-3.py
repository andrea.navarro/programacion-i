#Definir la función A con nombre de función pasada por parámetro
def funcionA(funcionParametro):
    print("Ejecutando función A")
    #Imprimir el nombre de la función pasada por parámetro
    print("Llamando a función pasada por parámetro "+ funcionParametro.__name__)
    #Llamar a la función por el nombre pasado por parámetro
    funcionParametro()

#Definir la función B
def funcionB():
   print("Ejecutando función B")

fb = funcionB
#Llamar a la función A pasando por parámetro el nombre de la función B
funcionA(fb)
