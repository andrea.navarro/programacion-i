#Define la función
def saludar():
	print("Hola!")

#Iguala el valor del nombre de la función a una nueva variable
saludo = saludar

#Llama a la función por el nombre con la que fue definida
saludar()
#Llama a la función por el nombre de la nueva variable
saludo()
