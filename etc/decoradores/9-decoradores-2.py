#Definir función del decorador que recibe el nombre de la función a ejecutar
def saludar(funcion):
	#Definir el wrapper que recibe como parámetro el pasado por la función decorada
    def saludo(parametro):
        print("Buenos días")
        #Llamar a la función decorada
        funcion(parametro)
        print("Hasta luego!")
    #Devolver el wrapper
    return saludo

#Definir función decoradora
@saludar
#Definir función decorada
def presentarse(nombre):
    print("Mi nombre es "+nombre)

#Llamar función decorada
presentarse("María")
