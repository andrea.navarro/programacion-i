#Definir la función A con nombre de función pasada por parámetro
def funcionA(funcionParametro):
    print("Ejecutando función A")
    print("Llamando a función pasada por parámetro")
    #Llamar a la función por el nombre pasado por parámetro
    funcionParametro()

#Definir la función B
def funcionB():
   print("Ejecutando función B")
#Igualar una nueva variable al nombre de la función B
fb = funcionB
#Llamar a la función A pasando por parámetro el nuevo nombre referenciando a la función B
funcionA(fb)
