#Definir función del decorador que recibe el nombre de la función a ejecutar
def saludar(funcion):
	#Definir el wrapper que recibe como parámetro el pasado por la función decorada
    def saludo(*args, **kwargs):
        print("Buenos días")
        #Llamar a la función decorada
        funcion(*args, **kwargs)
        print("Hasta luego!")
    #Devolver el wrapper
    return saludo

#Definir función decoradora
@saludar
#Definir función decorada
def presentarse(nombre):
    print("Mi nombre es "+nombre)

#Definir función decoradora
@saludar
#Definir función decorada
def contarHistoria(titulo, autor):
    print("Quiero contarte una historia titulada '"+titulo+"' escrita por "+autor+" ....")

#Definir función decoradora
@saludar
#Definir función decorada
def mostrarEstado():
    print("Todo está bien")

#Llamar función decorada
presentarse("María")
contarHistoria("La isla del tesoro","Robert Louis Stevenson" )
mostrarEstado()
