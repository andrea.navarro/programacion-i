import { Component } from '@angular/core';
//Importar servicio
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //Cargar servicio en el constructor
  constructor(public authService: AuthService) { }
  title = 'frontend-angular';

  //Llama a la funcion de deloguear del servicio
  logout() {
     this.authService.logout();
  }
}
