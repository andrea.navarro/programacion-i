//Importar modelo
import {Professor} from './professor.model';
//Importar observable
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
//Importar librerías
import {HttpClientModule, HttpClient, HttpErrorResponse} from '@angular/common/http';
//Importar URL de la API
import {API_URL} from '../env';

//Se decora la clase como un Injectable
@Injectable({
  //Permite que el servicio pueda inyectarse en toda la aplicación
  providedIn: 'root'
})
export class ProfessorService {

  //Agregar el cliente al constructor
  constructor(private http: HttpClient) { }

  //Función que obtiene los profesores
  getProfessors(): Observable<Professor[]> {
    //return of(this.professors);
    //Hacer request a la API
    return this.http.get<Professor[]>(API_URL+'/professors')
  }

  deleteProfessor(id){
    return this.http.delete<Professor>(API_URL+'/professor/'+id);
  }

  //Función para el manejo de errores HTTP
  private static _handleError(err: HttpErrorResponse | any) {
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }


}
