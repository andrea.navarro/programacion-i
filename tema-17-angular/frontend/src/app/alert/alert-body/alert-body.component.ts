import { Component, OnInit, Input } from '@angular/core';
//Importar clase que define tipos de alerta
import { AlertType } from '../alert.enum';
//Importar animaciones
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-alert-body',
  templateUrl: './alert-body.component.html',
  styleUrls: ['./alert-body.component.scss'],
  //cargar animaciones
  animations: [
    //Animación hideAlert
    trigger('hideAlert', [
    state('false', style({ opacity: 1 })),
    state('true', style({ opacity: 0 })),
    transition('false => true', animate('.5s'))
    ])
  ]
})

export class AlertBodyComponent implements OnInit {
  //Variable que guarda el mensaje
  private _message: string;
  //Variable que se setea a true al cerrar alerta
  hideAlert = false;
  //Estilod de la alerta
  bootstrapStyle: string;

  //Método que obtiene el mensaje de la alerta
  @Input() get message(): string {
    return this._message;
  }

  //Método que carga el mensaje
  set message(value: string) {
    this._message = value;
    this.hideAlert = false;
    //Cerrar automáticamente después de 5 segundos
    setTimeout(() => {
    this.hideAlertAnimator();
  }, 5000);
  }

  //Método que setea el tipo de alerta
  @Input() set type(value: AlertType) {
  switch (+value) {
    case AlertType.success:
    this.bootstrapStyle = 'success';
    break;
    case AlertType.warning:
    this.bootstrapStyle = 'warning';
    break;
    case AlertType.error:
    this.bootstrapStyle = 'danger';
    break;
    default:
    this.bootstrapStyle = 'info';
    break;
    }
  }

  //Método que llama a la animación que cierra la alerta
  hideAlertAnimator() {
    if (this.hideAlert === false) {
      this.hideAlert = true;
    }
  }

  //Método de cierre de la alerta
  close() {
    this.hideAlertAnimator();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
