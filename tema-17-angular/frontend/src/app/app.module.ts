import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Importar módulo de formulario
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfessorComponent } from './professor/professor.component';
import { ProjectComponent } from './project/project.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProjectDetailComponent } from './project/project-detail/project-detail.component';
import { ProfessorDetailComponent } from './professor/professor-detail/professor-detail.component';
//Importar módulo de HTTP e interceptor
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProjectAddComponent } from './project/project-add/project-add.component';
import { ProjectEditComponent } from './project/project-edit/project-edit.component';
import { LoginComponent } from './login/login.component';
import { AuthInterceptorService } from './auth-interceptor.service';
//Cargar módulo de alerta
import { AlertModule } from './alert/alert.module';

//Instalar con ng add @angular/material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
//Importar breadcrumbs
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfessorComponent,
    ProjectComponent,
    ProjectDetailComponent,
    ProfessorDetailComponent,
    ProjectAddComponent,
    ProjectEditComponent,
    LoginComponent,
    BreadcrumbComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    //Importar módulo de alerta
    AlertModule,
    //Importar modulos de iconos
    BrowserAnimationsModule,
    MatIconModule,
    ],
  providers: [
    //Cargar servicios de intercepción
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
