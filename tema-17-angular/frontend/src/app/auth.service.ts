import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClientModule, HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import {API_URL} from './env';
//Importar alerta
import { AlertService } from './alert/alert.service';
import { AlertType } from './alert/alert.enum';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
//Atributo donde se guardará el token
token;

  constructor(
    private http: HttpClient,
    private router: Router,
    private alert: AlertService
    ) { }

  //Método que realiza la request de logim
  login(email: string, password: string) {
    this.http.post(API_URL + '/auth/login', {email: email,password: password})
    .subscribe(
      (response) => {
        //Redireccionar a home
          this.router.navigate(['']);
          //Guardar el token
          localStorage.setItem('token', response['access_token']);

       },
       (error) => {
         var error_msj ="";
         if(error.status==404){
           error_msj = "User does not exist";
         }else{
           error_msj = error.error;
         }
         //Cargar alerta
         this.alert.add(AlertType.error, error_msj);             
       })
    }

  //Método de logout
  logout() {
    //Vaciar el token
    localStorage.removeItem('token');
    //Redireccionar a login
    this.router.navigate(['login']);
  }

  //Método que indica si el usuario está o no autenticado
  public get isAuthenticated(): boolean {
    return (localStorage.getItem('token') !== null);
  }

  //Función para el manejo de errores HTTP
  private static _handleError(err: HttpErrorResponse | any) {
    console.log("Ocurrio un error");
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }


}
