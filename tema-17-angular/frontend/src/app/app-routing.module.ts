import { NgModule } from '@angular/core';
import {LoginComponent } from './login/login.component';
// Importar componente de professor
import {ProfessorComponent } from './professor/professor.component';
import {ProfessorDetailComponent} from './professor/professor-detail/professor-detail.component'
// Importar componente de proyecto
import {ProjectComponent } from './project/project.component';
//Importar sub-componentes
import {ProjectAddComponent } from './project/project-add/project-add.component';
import {ProjectEditComponent } from './project/project-edit/project-edit.component';
//Agregar CanActivate
import { Routes, RouterModule,CanActivate } from '@angular/router';
//Importar servicio de protección de rutas
import {
  AuthGuardService as AuthGuard
} from './auth-guard.service';


const routes: Routes = [
   //Home
   {
   path: '',
   redirectTo: '/',
   pathMatch: 'full',
   data: {breadcrumb: 'Home'}
   },
   {
    path: '',
    data: { breadcrumb: 'Home' },
    children: [
      //Rutas de Professor
      {
        path: 'login',
        data: {breadcrumb: 'Login'},
        children: [
            {
                  path: '',
                  component: LoginComponent,
            },

          ]
        },
        //Rutas de Professor
        {
          path: 'professor',
          data: {breadcrumb: 'Professor'},
          children: [
              {
                    path: '',
                    component: ProfessorComponent,
                    //Asignar servicio de protección
                    canActivate: [AuthGuard]
              },

            ]
          },
            //Rutas de Proyectos
          {
            path: 'project',
            data: {breadcrumb: 'Project'},
            children: [
                {
                      path: '',
                      component: ProjectComponent,
                      canActivate: [AuthGuard]
                },
                //Agregar ruta para formulario
                {
                      path: 'add',
                      component: ProjectAddComponent,
                      data: {breadcrumb: 'Add'},
                      canActivate: [AuthGuard]
                },
                {
                      path: 'edit/:id',
                      component: ProjectEditComponent,
                      data: {breadcrumb: 'Edit'},
                      canActivate: [AuthGuard]
                },              
              ]
            },
    ],
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
