import { Component, OnInit} from '@angular/core';
//Importar Servicio y modelo de Proyecto
import {ProjectService} from '../project.service';
import {Project} from '../project.model';
//Importar Servicio y modelo de Professor
import {ProfessorService} from '../../professor/professor.service';
import {Professor} from '../../professor/professor.model';
//Importar funciones de ruteo
import {Router} from "@angular/router";

import { AlertService } from '../../alert/alert.service';
import { AlertType } from '../../alert/alert.enum';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.scss'],
  providers: [
      AlertService
    ]
})
export class ProjectAddComponent implements OnInit {
  //Variable que se modifica al enviar el formulario
  submitted = true;
  //Variable que contendrá la lista de profesores para el desplegable
  professors: Professor[];
  //Crear proyecto vacío
  project : Project = new Project('', null,null,null);

  constructor(private router: Router,
              private professorService: ProfessorService,
              private projectService: ProjectService,
              private alertService: AlertService) { }

  ngOnInit(): void {
    //Obtener lista de profesores para cargar en el select
    this.getProfessors();
  }

  //Llamar al servicio cuando se envía el formulario
  onSubmit() {
    this.submitted = true;
    this.projectService.createProject(this.project)
      .subscribe( data => {
        //Redireccionar a projectos
        this.router.navigate(['project']);
        this.alertService.add(AlertType.success, 'Project Added');
      });
   }

 //Obtener lista de profesores
  getProfessors():void{
    this.professorService.getProfessors()
      .subscribe (professors => this.professors = professors["professors"]);
    }

}
