import {Professor} from '../professor/professor.model';
export class Project {
  constructor(
    public name: string,
    public year: number,
    public professorId: number,
    public professor: Professor,
    public id? : number,
  ){}
}
