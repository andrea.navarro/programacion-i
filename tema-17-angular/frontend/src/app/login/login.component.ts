import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//Importar servicio
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email = '';
  password = '';

 constructor(private authService: AuthService) { }

 //Llama a la función de login del servicio
 login() {
   this.authService.login(this.email, this.password)
 }

  ngOnInit(): void {
  }

}
