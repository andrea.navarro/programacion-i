from flask import Blueprint, render_template

#Crear Blueprint
professor = Blueprint('professor', __name__, url_prefix='/professor')

@professor.route('/')
def index():
    #Mostrar template
    return render_template('professor_list.html' )

@professor.route('/view/<int:id>')
def view(id):
    #Mostrar template
    return render_template('professor_view.html' )
