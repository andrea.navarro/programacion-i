from flask import Blueprint, render_template

#Crear Blueprint
professor = Blueprint('professor', __name__, url_prefix='/professor')


PROFESSORS = [
            {"id":0,"firstname":"Pedro","lastname":"Sosa","email":"p.sosa@escuela.com"},
            {"id":1,"firstname":"María","lastname":"Moreno","email":"m.moreno@escuela.com", "private":True},
            {"id":2,"firstname":"Julieta","lastname":"Gonzales","email":"j.gonzales@escuela.com"}
            ]

@professor.route('/')
def index():
    #Mostrar template
    return render_template('professor_list.html',professors = PROFESSORS )

@professor.route('/view/<int:id>')
def view(id):

    #Mostrar template
    return render_template('professor_view.html', professor = PROFESSORS[id] )
