import os
from flask import Flask
from dotenv import load_dotenv
#Importar librería flask_restful
from flask_restful import Api
#Importar SQLAlchemy
from flask_sqlalchemy import SQLAlchemy
#Importar Flask JWT
from flask_jwt_extended import JWTManager


#Inicializar API de Flask Restful
api = Api()
#Inicializar SQLAlchemy
db = SQLAlchemy()
#Inicializar
jwt = JWTManager()

def create_app():
    app = Flask(__name__)
    load_dotenv()

    #Si no existe el archivo de base de datos crearlo (solo válido si se utiliza SQLite)
    if not os.path.exists(os.getenv('DATABASE_PATH')+os.getenv('DATABASE_NAME')):
        os.mknod(os.getenv('DATABASE_PATH')+os.getenv('DATABASE_NAME'))

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    #Url de configuración de base de datos
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////'+os.getenv('DATABASE_PATH')+os.getenv('DATABASE_NAME')
    db.init_app(app)

    #Importar directorio de recursos
    import main.resources as resources

    #Cargar a la API el Recurso Profesors e indicar ruta
    api.add_resource(resources.ProfessorsResource, '/professors')
    #Cargar a la API el Recurso Profesor e indicar ruta
    api.add_resource(resources.ProfessorResource, '/professor/<id>')
    #Cargar a la API el Recurso Profesors e indicar ruta
    api.add_resource(resources.ProjectsResource, '/projects')
    #Cargar a la API el Recurso Profesor e indicar ruta
    api.add_resource(resources.ProjectResource, '/project/<id>')
    #Cargar la aplicación en la API de Flask Restful
    api.init_app(app)

    #Cargar clave secreta
    app.config['JWT_SECRET_KEY'] = os.getenv('JWT_SECRET_KEY')
    #Cargar tiempo de expiración de los tokens
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = int(os.getenv('JWT_ACCESS_TOKEN_EXPIRES'))
    jwt.init_app(app)

    from main.auth import routes
    #Importar blueprint
    app.register_blueprint(routes.auth)

    return app
