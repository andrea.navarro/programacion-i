from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.models import ProfessorModel
from flask_jwt_extended import jwt_required, get_jwt_identity
from main.auth.decorators import admin_required

#Recurso Profesor
class Professor(Resource):
    #Obtener recurso
    @jwt_required()
    def get(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        return professor.to_json()
    #Eliminar recurso
    @admin_required
    def delete(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        db.session.delete(professor)
        db.session.commit()
        return '', 204
    #Modificar recurso
    @jwt_required()
    def put(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        data = request.get_json().items()
        for key, value in data:
            setattr(professor, key, value)
        db.session.add(professor)
        db.session.commit()
        return professor.to_json() , 201

#Recurso Profesores
class Professors(Resource):
    #Obtener lista de recursos
    @jwt_required()
    def get(self):
        #Página inicial por defecto
        page = 1
        #Cantidad de elementos por página por defecto
        per_page = 10
        professors = db.session.query(ProfessorModel)
        if request.get_json():
            filters = request.get_json().items()
            for key, value in filters:
                #Paginación
                if key =="page":
                    page = int(value)
                if key == "per_page":
                    per_page = int(value)
        #Obtener valor paginado
        professors = professors.paginate(page, per_page, True, 30)
        #Devolver además de los datos la cantidad de páginas y elementos existentes antes de paginar
        return jsonify({ 'professors': [professor.to_json() for professor in professors.items],
                  'total': professors.total,
                  'pages': professors.pages,
                  'page': page
                  })
