from .. import jwt, db
from flask import jsonify, request
from flask_jwt_extended import verify_jwt_in_request, get_jwt
from .. models import PermissionModel
from functools import wraps

#Decorador para restringir el acceso a usuarios por rol
def role_required():
    def decorator(fn):
        def wrapper(*args, **kwargs):
            #Verificar que el JWT es correcto
            verify_jwt_in_request()
            #Obtener claims de adentro del JWT
            claims = get_jwt()
            #Obtener recurso
            resource = request.endpoint
            #Obtener método
            method = fn.__name__
            #Obtenr el rol del usuario
            rol = claims['role']

            #Verificar si el método tiene restricciones
            exists = db.session.query(PermissionModel).filter(PermissionModel.resource == resource).first() is not None
            if exists:
                #Verificar si el usuario un rol permitido
                role_not_permitted = db.session.query(PermissionModel).filter(PermissionModel.resource == resource).filter(PermissionModel.method == method).filter(PermissionModel.rol == rol).scalar() is None
                #Si no se permite el rol dar error
                if role_not_permitted:
                    return 'Rol not allowed', 403
            #Acceder a la ruta
            return fn(*args, **kwargs)
        return wrapper
    return decorator

#Define el atributo que se utilizará para identificar el usuario
@jwt.user_identity_loader
def user_identity_lookup(professor):
    #Definir ID como atributo identificatorio
    return professor.id

#Define que atributos se guardarán dentro del token
@jwt.additional_claims_loader
def add_claims_to_access_token(professor):
    claims = {
        'role': professor.role,
        'id': professor.id,
        'email': professor.email
    }
    return claims
