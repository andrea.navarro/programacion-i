from .. import db

class Permission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rol = db.Column(db.String(100), nullable=False)
    resource = db.Column(db.String(100), nullable=False)
    method = db.Column(db.String(100), nullable=False)
    def __repr__(self):
        return '<Permission: %r %r %r >' % (self.rol, self.resource, self.method)
    
