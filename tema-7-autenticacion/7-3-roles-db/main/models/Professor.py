from .. import db
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

class Professor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    fechaNac = db.Column(db.DateTime, nullable=False)
    #Mail usado como nombre de usuario
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    #Contraseña que será el hash de la pass en texto plano
    password = db.Column(db.String(128), nullable=False)
    #Rol (En el caso que existan diferentes tipos de usuarios con diferentes permisos)
    role = db.Column(db.String(10), nullable=False, default="junior")
    #Relación
    projects = db.relationship("Project", back_populates="professor",cascade="all, delete-orphan")

    #Getter de la contraseña plana no permite leerla
    @property
    def plain_password(self):
        raise AttributeError('Password cant be read')
    #Setter de la contraseña toma un valor en texto plano
    # calcula el hash y lo guarda en el atributo password
    @plain_password.setter
    def plain_password(self, password):
        self.password = generate_password_hash(password)
    #Método que compara una contraseña en texto plano con el hash guardado en la db
    def validate_pass(self,password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<Professor: %r %r >' % (self.firstname, self.lastname)
    #Convertir objeto en JSON
    def to_json(self):
        professor_json = {
            'id': self.id,
            'firstname': str(self.firstname),
            'lastname': str(self.lastname),
            'fechaNac': str(self.fechaNac.strftime("%d-%m-%Y")),
            'email': str(self.email),

        }
        return professor_json
    @staticmethod
    #Convertir JSON a objeto
    def from_json(professor_json):
        id = professor_json.get('id')
        firstname = professor_json.get('firstname')
        lastname = professor_json.get('lastname')
        fechaNac = datetime.strptime(professor_json.get('fechaNac'), '%d-%m-%Y')
        email = professor_json.get('email')
        password = professor_json.get('password')
        role = professor_json.get('role')
        return Professor(id=id,
                    firstname=firstname,
                    lastname=lastname,
                    fechaNac=fechaNac,
                    email=email,
                    plain_password=password,
                    role=role,
                    )
