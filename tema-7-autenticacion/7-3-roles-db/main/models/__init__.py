from .Professor import Professor as ProfessorModel
from .Project import Project as ProjectModel
from .Permission import Permission as PermissionModel
