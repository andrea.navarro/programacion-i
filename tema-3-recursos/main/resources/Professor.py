from flask_restful import Resource
from flask import request

#Diccionario de prueba
PROFESSORS = {
    1: {'firstname': 'Pedro', 'lastname': 'Marco'},
    2: {'firstname': 'María', 'lastname': 'Sosa'},
}

#Recurso Profesor
class Professor(Resource):
    #Obtener recurso
    def get(self, id):
        #Verificar que exista un Profesor con ese Id en diccionario
        if int(id) in PROFESSORS:
            #Devolver professor correspondiente
            return PROFESSORS[int(id)]
        #Devolver error 404 en caso que no exista
        return '', 404
    #Eliminar recurso
    def delete(self, id):
        #Verificar que exista un Profesor con ese Id en diccionario
        if int(id) in PROFESSORS:
            #Eliminar professor del diccionario
            del PROFESSORS[int(id)]
            return '', 204
        return '', 404
    #Modificar recurso
    def put(self, id):
        if int(id) in PROFESSORS:
            professor = PROFESSORS[int(id)]
            #Obtengo los datos de la solicitud
            data = request.get_json()
            professor.update(data)
            return professor, 201
        return '', 404

#Recurso Profesores
class Professors(Resource):
    #Obtener lista de recursos
    def get(self):
        return PROFESSORS
    #Insertar recurso
    def post(self):
        #Obtener datos de la solicitud
        professor = request.get_json()
        id = int(max(PROFESSORS.keys())) + 1
        PROFESSORS[id] = professor
        return PROFESSORS[id], 201
