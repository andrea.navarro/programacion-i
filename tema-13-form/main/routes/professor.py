from flask import Blueprint, render_template, redirect, url_for
# Importar objeto Form
from ..forms.professor_form import ProfessorForm

#Crear Blueprint
professor = Blueprint('professor', __name__, url_prefix='/professor')


PROFESSORS = [
            {"id":0,"firstname":"Pedro","lastname":"Sosa","email":"p.sosa@escuela.com"},
            {"id":1,"firstname":"María","lastname":"Moreno","email":"m.moreno@escuela.com", "private":True},
            {"id":2,"firstname":"Julieta","lastname":"Gonzales","email":"j.gonzales@escuela.com"}
            ]

@professor.route('/')
def index():
    #Mostrar template
    return render_template('professor_list.html',professors = PROFESSORS )

@professor.route('/view/<int:id>')
def view(id):
    #Mostrar template
    return render_template('professor_view.html', professor = PROFESSORS[id] )

@professor.route('/create', methods=["POST","GET"])
def create():
    form = ProfessorForm() #Instanciar formulario
    if form.validate_on_submit(): #Si el formulario ha sido enviado y es validado correctamente
        print("Agregado")
        print(form.firstname.data)
        print(form.lastname.data)
        print(form.email.data)
        print(form.password.data)
        return redirect(url_for('professor.index')) #Redirecciona a lista
    return render_template('professor_form.html', form = form) #Muestra el formulario
