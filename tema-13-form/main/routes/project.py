from flask import Blueprint, render_template, current_app, redirect, url_for
# Importar objeto Form
from ..forms.project_form import ProjectForm

project = Blueprint('project', __name__, url_prefix='/project')

PROFESSORS = [
            {"id":0,"firstname":"Pedro","lastname":"Sosa","email":"p.sosa@escuela.com"},
            {"id":1,"firstname":"María","lastname":"Moreno","email":"m.moreno@escuela.com", "private":True},
            {"id":2,"firstname":"Julieta","lastname":"Gonzales","email":"j.gonzales@escuela.com"}
            ]

@project.route('/create', methods=["POST","GET"])
def create():
    form = ProjectForm() #Instanciar formulario
    # Crear lista de profesores
    professors = [(professor['id'], professor['lastname']+" "+professor['firstname']) for professor in PROFESSORS]
    # Agregar lista de profesores al elemento select del formulario
    form.professorId.choices = professors
    if form.validate_on_submit(): #Si el formulario ha sido enviado y es validado correctamente
        print(form.name) # Campo HTML
        print(form.name.label) # CAMPO HTML de etiqueta
        print(form.name.data) # Valor del campo
        print(form.name.errors) # Lista de errores de validación para el campo
        print(form.professorId.choices)
        return redirect(url_for('professor.index')) #Redirecciona a lista
    return render_template('project_form.html', form = form) #Muestra el formulario
