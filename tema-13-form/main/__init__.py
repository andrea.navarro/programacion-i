import os
from flask import Flask
from dotenv import load_dotenv
from flask_wtf import CSRFProtect #importar para proteccion CSRF

# Inicializar módulos CSRF
csrf = CSRFProtect()

def create_app():
    app = Flask(__name__)
    load_dotenv()
    # Importar clave secreta
    app.config["SECRET_KEY"] = os.getenv('SECRET_KEY')
    #Inicializar protección csrf
    csrf.init_app(app)
    #Importar Blueprints
    from main.routes import main, professor, project
    app.register_blueprint(routes.main.main)
    app.register_blueprint(routes.professor.professor)
    app.register_blueprint(routes.project.project)
    return app
