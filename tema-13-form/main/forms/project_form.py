# - *- coding: utf- 8 - *-
from flask_wtf import FlaskForm #Importa funciones de formulario
from wtforms import PasswordField, SubmitField, StringField, SelectField, HiddenField#Importa campos
from wtforms.fields.html5 import EmailField, DateTimeField #Importa campos HTML
from wtforms import validators #Importa validaciones

class ProjectForm(FlaskForm):

    #Definición de campo String
    name = StringField('Name',
    [
        #Definición de validaciones
        validators.Required(message = "Name is required")
    ])

    year = DateTimeField('Year', format='%Y')

    professorId = SelectField('Professor', coerce=int)
    #Definición de campo submit
    submit = SubmitField("Send")
