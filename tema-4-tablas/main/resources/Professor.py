from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.models import ProfessorModel


#Recurso Profesor
class Professor(Resource):
    #Obtener recurso
    def get(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        return professor.to_json()
    #Eliminar recurso
    def delete(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        db.session.delete(professor)
        db.session.commit()
        return '', 204
    #Modificar recurso
    def put(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        data = request.get_json().items()
        for key, value in data:
            setattr(professor, key, value)
        db.session.add(professor)
        db.session.commit()
        return professor.to_json() , 201

#Recurso Profesores
class Professors(Resource):
    #Obtener lista de recursos
    def get(self):
        professors = db.session.query(ProfessorModel).all()
        return jsonify([professor.to_json_short() for professor in professors])

    """
            list_prof = []
            for professor in professors:
                list_prof.append(professor.to_json())
            return jsonify(list_prof)
    """


    #Insertar recurso
    def post(self):
        professor = ProfessorModel.from_json(request.get_json())
        db.session.add(professor)
        db.session.commit()
        return professor.to_json(), 201
