from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.models import ProjectModel, ProfessorModel

#Recurso Proyecto
class Project(Resource):
    #Obtener recurso
    def get(self, id):
        project = db.session.query(ProjectModel).get_or_404(id)
        return project.to_json()
    #Eliminar recurso
    def delete(self, id):
        project = db.session.query(ProjectModel).get_or_404(id)
        db.session.delete(project)
        db.session.commit()
        return '', 204
    #Modificar recurso
    def put(self, id):
        project = db.session.query(ProjectModel).get_or_404(id)
        data = request.get_json().items()
        for key, value in data:
            setattr(project, key, value)
        db.session.add(project)
        db.session.commit()
        return project.to_json() , 201

#Recurso Proyectos
class Projects(Resource):
    #Obtener lista de recursos
    def get(self):
        #Obtener valores del request
        filters =  request.data
        projects = db.session.query(ProjectModel)
        #Verificar si hay filtros
        if filters:
            #Recorrer filtros
            for key, value in request.get_json().items():
                if key == "professorId":
                    projects = projects.filter(ProjectModel.professorId == value)
                if key == "year":
                    projects = projects.filter(ProjectModel.year == value)
                if key == "professor_firstname":
                    projects = projects.filter(ProjectModel.professor.has(ProfessorModel.firstname.like("%"+value+"%")))
        projects = projects.all()
        return jsonify({ 'projects': [project.to_json() for project in projects] })
    #Insertar recurso
    def post(self):
        project = ProjectModel.from_json(request.get_json())
        try:
            db.session.add(project)
            db.session.commit()
        except:
            return 'Formato no correcto', 400
        return project.to_json(), 201
