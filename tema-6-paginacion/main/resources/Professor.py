from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.models import ProfessorModel, ProjectModel
from sqlalchemy import func


#Recurso Profesor
class Professor(Resource):
    #Obtener recurso
    def get(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        return professor.to_json()
    #Eliminar recurso
    def delete(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        db.session.delete(professor)
        db.session.commit()
        return '', 204
    #Modificar recurso
    def put(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        data = request.get_json().items()
        for key, value in data:
            setattr(professor, key, value)
        db.session.add(professor)
        db.session.commit()
        return professor.to_json() , 201

#Recurso Profesores
class Professors(Resource):
    #Obtener lista de recursos
    def get(self):
        #Página inicial por defecto
        page = 1
        #Cantidad de elementos por página por defecto
        per_page = 10
        professors = db.session.query(ProfessorModel)
        if request.get_json():
            filters = request.get_json().items()
            for key, value in filters:
                #Paginación
                if key =="page":
                    page = int(value)
                if key == "per_page":
                    per_page = int(value)
                if key == "nprojects[gt]":
                    professors=professors.outerjoin(ProfessorModel.projects).group_by(ProfessorModel.id).having(func.count(ProjectModel.id) >= value)
                if key == "firstname":
                    professors=professors.filter(ProfessorModel.firstname.like("%"+value+"%"))
                if key == "sortby":
                    if value == "nprojects[desc]":
                        professors=professors.outerjoin(ProfessorModel.projects).group_by(ProfessorModel.id).order_by(func.count(ProjectModel.id).desc())
                    if value == "nprojects":
                        professors=professors.outerjoin(ProfessorModel.projects).group_by(ProfessorModel.id).order_by(func.count(ProjectModel.id))
                    if value == "firstname":
                        professors=professors.order_by(ProfessorModel.firstname)
                    if value == "avg_year":
                        professors=professors.outerjoin(ProfessorModel.projects).group_by(ProfessorModel.id).order_by(func.avg(ProjectModel.year).desc())
        #Obtener valor paginado
        professors = professors.paginate(page, per_page, False, 30)
        #Devolver además de los datos la cantidad de páginas y elementos existentes antes de paginar
        return jsonify({ 'professors': [professor.to_json() for professor in professors.items],
                  'total': professors.total,
                  'pages': professors.pages,
                  'page': page
                  })
    #Insertar recurso
    def post(self):
        professor = ProfessorModel.from_json(request.get_json())
        db.session.add(professor)
        db.session.commit()
        return professor.to_json(), 201
