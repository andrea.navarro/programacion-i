# Programación I

Repositorio destinado a códigos de ejemplo para la cátedra Programación I.

# Instalación

Crear un archivo .env a partir de .env-example
Setear configuraciones correspondientes 
Ejecutar install.sh

# Correr servidor

Ejecutar boot.sh
