#Cambiar el nombre en la importación para clarificar y evitar conflictos
from .Professor import Professor as ProfessorResource
from .Professor import Professors as ProfessorsResource
from .Project import Project as ProjectResource
from .Project import Projects as ProjectsResource
