from .. import db
from datetime import datetime

class Professor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    fechaNac = db.Column(db.DateTime, nullable=False)
    #Relación
    projects = db.relationship("Project", back_populates="professor",cascade="all, delete-orphan")
    def __repr__(self):
        return '<Professor: %r %r >' % (self.firstname, self.lastname)
    #Convertir objeto en JSON
    def to_json(self):
        professor_json = {
            'id': self.id,
            'firstname': str(self.firstname),
            'lastname': str(self.lastname),
            'fechaNac': str(self.fechaNac.strftime("%d-%m-%Y"))

        }
        return professor_json
    def to_json_complete(self):
        projects = [project.to_json() for project in self.projects]
        professor_json = {
            'id': self.id,
            'firstname': str(self.firstname),
            'lastname': str(self.lastname),
            'fechaNac': str(self.fechaNac.strftime("%d-%m-%Y")),
            'projects':projects

        }
        return professor_json
    @staticmethod
    #Convertir JSON a objeto
    def from_json(professor_json):
        id = professor_json.get('id')
        firstname = professor_json.get('firstname')
        lastname = professor_json.get('lastname')
        fechaNac = datetime.strptime(professor_json.get('fechaNac'), '%d-%m-%Y')
        return Professor(id=id,
                    firstname=firstname,
                    lastname=lastname,
                    fechaNac=fechaNac
                    )
