Bienvenido {{professor.firstname}} {{professor.lastname}}!

Gracias por registrarte en nuestro sistema.

Con nuestro servicio podras:

- Cargar tus proyectos
- Eliminar proyectos
- Buscar proyectos
